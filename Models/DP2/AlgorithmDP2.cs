﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Models.DP2
{
    class AlgorithmDP2
    {
        public int[,] Matrix { get; set; }
        public int Size { get; set; }
        public List<ItemG> ItemsG { get; set; }
        public List<string> AllSets { get; set; }
        public bool Test { get; set; }
        private int actualNumberOfSet = 0;
        public ItemG Result { get; set; }


        public AlgorithmDP2(int[,] matrix, int size, bool test = false)
        {
            Size = size;
            Matrix = new int[size, size];
            Matrix = matrix;
            AllSets = new List<string>();
            ItemsG = new List<ItemG>();
            Test = test;
        }

        public void Start()
        {
            GenerateAllSets();

            string firstSet = "";
            for (int i = 0; i < Size; i++)
            {
                firstSet += 0;
            }
            //pierwsze wartości
            for (int i = 1; i < Size; i++)
            {
                ItemsG.Add(new ItemG(Matrix, Size, i, firstSet));
            }
            actualNumberOfSet++;
  
            //klejne wartości
            while(actualNumberOfSet < Size)
            {
                List<string> sets = FindSetWith(actualNumberOfSet);
                foreach (string set in sets)
                {
                    for (int i = 1; i < Size; i++)
                    {
                        if (set[i] == '0')
                        {
                            ItemsG.Add(new ItemG(Matrix, Size, ItemsG, i, set));
                        }
                    }
                }

                actualNumberOfSet++;
            }

            
            string finalSet = "0";
            for (int i = 1; i < Size; i++)
            {
                finalSet += 1;
            }
            Result = new ItemG(Matrix, Size, ItemsG, 0, finalSet);

            if (!Test)
            {
                //ShowAllG();       
                Console.WriteLine("Wynik to: " + Result.G);
                ShowPath();
                Console.ReadKey();
            }



        }

        private void ShowPath()
        {
            List<int> path = new List<int>();
            ItemG current = Result;
            path.Add(0);
            for (int i = 0; i < Size-1; i++)
            {
                path.Add(current.P);
                StringBuilder newSet = new StringBuilder(current.Set);
                newSet[current.P] = '0';
                current = FindG(current.P, newSet.ToString());
            }

            foreach (int item in path)
            {
                Console.Write(item + "-->");
            }
            Console.Write(0);

        }

        private List<string> FindSetWith(int sizeOfSet)
        {
            List<string> result = new List<string>();

            foreach (string set in AllSets)
            {
                int couter = 0;
                for (int i = 1; i < Size; i++)
                {
                    if (set[i] == '1') couter++;
                }
                if (couter == sizeOfSet)
                {
                    result.Add(set);
                }
            }

            return result;
        }

        public void GenerateAllSets()
        {
            int max = (int)Math.Pow(2, Size - 1);
            for (int i = 0; i < max; i++)
            {
                AllSets.Add(0 + GetIntBinaryString(i));
            }
        }

        private string GetIntBinaryString(int n)
        {
            char[] b = new char[Size - 1];
            int pos = Size - 2;
            int i = 0;

            while (i < Size - 1)
            {
                if ((n & (1 << i)) != 0)
                {
                    b[pos] = '1';
                }
                else
                {
                    b[pos] = '0';
                }
                pos--;
                i++;
            }
            return new string(b);
        }

        private string GenerateSet(List<int> set)
        {
            set = new List<int>();
            string result = "";
            for (int i = 0; i < Size; i++)
            {
                if (set.Contains(i)) result += 1;
                else result += 0;
            }
            return result;
        }

        private void ShowAllG()
        {
            for (int i = 0; i < ItemsG.Count; i++)
            {
                Console.WriteLine("Dla g("+ItemsG[i].EndVertex +", "+ ItemsG[i].Set+") = "+ "g: " + ItemsG[i].G + " p: " + ItemsG[i].P);
            }
        }

        private ItemG FindG(int endVertex, string set)
        {
            return ItemsG.Find(a => (a.EndVertex == endVertex) && (a.Set == set));
        }

    }

    class ItemG
    {
        public int[,] Matrix { get; set; }
        public int Size { get; set; }
        public List<ItemG> Items { get; set; }

        public bool FirstStep { get; set; }
        public int EndVertex { get; set; }
        public string Set { get; set; }

        public int G { get; set; }
        public int P { get; set; }

        public ItemG(int[,] matrix, int size, List<ItemG> items, int endVertex, string set)
        {
            Size = size;
            Matrix = new int[size, size];
            Matrix = matrix;
            Items = items;

            FirstStep = false;
            EndVertex = endVertex;
            Set = set;

            Create();
        }

        public ItemG(int[,] matrix, int size, int endVertex, string set)
        {
            Size = size;
            Matrix = new int[size, size];
            Matrix = matrix;

            FirstStep = true;
            EndVertex = endVertex;
            Set = set;

            Create();
        }

        private void Create()
        {
            if (FirstStep)
            {
                G = Matrix[EndVertex, 0];
                P = 1;
            }
            else
            {
                int min = Int32.MaxValue;
                for (int i = 1; i < Set.Length; i++)
                {
                    if (Set[i] == '1')
                    {
                        StringBuilder newSet = new StringBuilder(Set);
                        newSet[i] = '0';

                        int result = Matrix[EndVertex, i] + FindG(i, newSet.ToString());

                        if (min > result)
                        {
                            min = result;
                            P = i;
                        }
                    }
                }
                G = min;
            }
        }

        private int FindG(int endVertex, string set)
        {
            return Items.Find(a => (a.EndVertex == endVertex) && (a.Set == set)).G;
        }
    }
}
