﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Models.BF
{
    class AlgorithmBF
    {
        public int[,] Matrix { get; set; }
        public int Size { get; set; }

        public int[] Solution;
        public int ValueSolution = Int32.MaxValue;
        public int[] Posibilities;
        public bool Test = false;


        public AlgorithmBF(int[,] matrix, int size, bool test = false)
        {
            Size = size;
            Matrix = new int[size, size];
            Matrix = matrix;
            Solution = new int[size];
            Posibilities = new int[size];
            Test = test;
        }

        public void Start()
        {
            for (int i = 0; i < Size; i++)
            {
                Posibilities[i] = i;
            }
            prnPermut(Posibilities, 1, Size-1);

            if (!Test)
            {
                ShowSolution();
                Console.ReadKey();
            }

        }

        public void ShowSolution()
        {
            Console.WriteLine("Najkrótsza droga ma wartość: " + ValueSolution);

            for (int i = 0; i < Size; i++)
            {
                if (i == Size-1)
                {
                    Console.Write(Solution[i] + " --> 0" );
                }
                else
                {
                    Console.Write(Solution[i] + " --> ");
                }
            }
        }

        public void swapTwoNumber(ref int a, ref int b)
        {
            int temp = a;
            a = b;
            b = temp;
        }

        public void prnPermut(int[] list, int current, int size)
        {
            int i;
            if (current == size)
            {
                //for (i = 0; i <= size; i++)
                //{
                //   Console.Write("{0}", list[i]);
                //}
                isSolution(list);

                //Console.WriteLine();
            }
            else
            {
                for (i = current; i <= size; i++)
                {
                    swapTwoNumber(ref list[current], ref list[i]);
                    prnPermut(list, current + 1, size);
                    swapTwoNumber(ref list[current], ref list[i]);
                }
            }
        }

        private void isSolution(int[] list)
        {
            int result = 0;
            for (int i = 0; i < Size; i++)
            {
                if(i+1 != Size)
                {
                    result += Matrix[list[i], list[i + 1]];
                }
                else
                {
                    result += Matrix[list[i], 0];
                }
            }
            //Console.Write(" --> " + result);


            if (result < ValueSolution)
            {
                ValueSolution = result;
                for (int i = 0; i < Size; i++)
                {
                    Solution[i] = list[i];
                }
            }

        }
    }

}
