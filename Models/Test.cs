﻿using Project2.Models.BAndB;
using Project2.Models.BF;
using Project2.Models.DP2;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Project2.Models
{
    class Test
    {
        public int[,] Matrix { get; set; }
        public int Size { get; set; }
        public string Algorithm { get; set; }


        Random rnd = new Random();

        Stopwatch stopwatch = new Stopwatch();

        public Test(int size, string algorithm)
        {
            Size = size;
            Algorithm = algorithm;
        }

        public double StartTest()
        {
            Matrix = new int[Size, Size];        
            return MakeTest();
        }

        private void GenerateGraph()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if(i != j)
                    {
                        Matrix[i, j] = rnd.Next(1, 11);
                    }
                    else
                    {
                        Matrix[i, j] = 0;
                    }
                }
            }
        }

        private double MakeTest()
        {
            double ms = 0;
            if (Algorithm == "BF")
            {
                stopwatch.Reset();
                GenerateGraph();
                AlgorithmBF algorithmBF = new AlgorithmBF(Matrix, Size, true);
                stopwatch.Start();
                algorithmBF.Start();
                stopwatch.Stop();

                long ticks = stopwatch.ElapsedTicks;
                double ns = 1000000000.0 * (double)ticks / Stopwatch.Frequency;
                ms = ns / 1000000.0;
            }

            if (Algorithm == "B&B")
            {
                stopwatch.Reset();
                GenerateGraph();
                AlgorithmBAndB algorithmBAndB = new AlgorithmBAndB(Matrix, Size, true);
                stopwatch.Start();
                algorithmBAndB.Start();
                stopwatch.Stop();

                long ticks = stopwatch.ElapsedTicks;
                double ns = 1000000000.0 * (double)ticks / Stopwatch.Frequency;
                ms = ns / 1000000.0;
            }

            if (Algorithm == "DP")
            {
                stopwatch.Reset();
                GenerateGraph();
                AlgorithmDP2 algorithmDP = new AlgorithmDP2(Matrix, Size, true);
                stopwatch.Start();
                algorithmDP.Start();
                stopwatch.Stop();

                long ticks = stopwatch.ElapsedTicks;
                double ns = 1000000000.0 * (double)ticks / Stopwatch.Frequency;
                ms = ns / 1000000.0;
            }

            return ms;


        }
    }
}