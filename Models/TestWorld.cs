﻿using System;
using System.IO;

namespace Project2.Models
{
    class TestWorld
    {
        public int Size { get; set; }


        public void StartBF()
        {
            Console.WriteLine("Test -> START");
            Size = 7;
            for (int i = 0; i < 7; i++)
            {
                Console.WriteLine("... " + Size);
                BFTests();
                Size ++;
            }
            Console.WriteLine("Test -> STOP");
            Console.ReadKey();
        }

        public void StartBAndB()
        {
            Console.WriteLine("Test -> START");
            Size = 7;
            for (int i = 0; i < 7; i++)
            {
                Console.WriteLine("... " + Size);
                BAndBTests();
                Size++;
            }
            Console.WriteLine("Test -> STOP");
            Console.ReadKey();
        }

        public void StartDP()
        {
            Console.WriteLine("Test -> START");
            Size = 7;
            for (int i = 0; i < 7; i++)
            {
                Console.WriteLine("... " + Size);
                DPTests();
                Size++;
            }
            Console.WriteLine("Test -> STOP");
            Console.ReadKey();
        }

        public void BFTests()
        {
            using (StreamWriter sw = File.AppendText("BF.txt"))
            {

                double result = 0;
                sw.WriteLine("Algorytm: BF/"+Size);

                if (Size < 13)
                {
                    for (int i = 0; i < 100; i++)
                    {
                        Test test = new Test(Size, "BF");
                        var oneTestResult = test.StartTest();
                        result += oneTestResult;
                    }

                    sw.WriteLine(result / 100);
                }
                else
                {
                    Test test = new Test(Size, "BF");
                    var oneTestResult = test.StartTest();
                    result += oneTestResult;                
                    sw.WriteLine(result);
                }

            }
        }

        public void BAndBTests()
        {
            using (StreamWriter sw = File.AppendText("BAndB.txt"))
            {
                double result = 0;
                sw.WriteLine("Algorytm: B&B/" + Size);

                for (int i = 0; i < 100; i++)
                {
                    Test test = new Test(Size, "B&B");
                    var oneTestResult = test.StartTest();
                    result += oneTestResult;
                }

                sw.WriteLine(result / 100);

            }
        }

        public void DPTests()
        {
            using (StreamWriter sw = File.AppendText("DP.txt"))
            {
                double result = 0;
                sw.WriteLine("Algorytm: DP/" + Size);

                for (int i = 0; i < 100; i++)
                {
                    Test test = new Test(Size, "DP");
                    var oneTestResult = test.StartTest();
                    result += oneTestResult;
                }

                sw.WriteLine(result / 100);

            }
        }


    }
}