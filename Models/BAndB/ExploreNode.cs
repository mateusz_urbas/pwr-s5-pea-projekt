﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Models.BAndB
{
    class ExploreNode
    {
        public int Position { get; set; }

        public int ParentNode { get; set; }
        public int ParentPosition { get; set; }

        public int Node { get; set; }

        public int[,] Matrix { get; set; }

        public bool IsExplored { get; set; }
        public int Cost { get; set; }

        private bool[] Visited { get; set; }

        public int Size { get; set; }

        public int Progress { get; set; }


        public ExploreNode(int position, int parentNode, int node, int[,] matrix, int cost, bool[] visited, int size, int progress, int parentPosition)
        {
            Size = size;
            Progress = progress;
            ParentPosition = parentPosition;
            initMatrix(matrix);

            Visited = new bool[Size];
            Cost = cost + Matrix[parentNode, node];
            Position = position;
            Node = node;
            initVisited(visited);

            ParentNode = parentNode;
           
            AddInfinityToMatrix();
            ReduceMatrix();
        }

        public ExploreNode(int[,] matrix, bool[] visited, int size)
        {
            Size = size;
            Progress = 0;
            ParentPosition = -1;
            initMatrix(matrix);
            initVisited(visited);

            Cost = 0;
            Position = 0;
            Node = 0;
            ParentNode = -1;
            
            ReduceMatrix();
        }

        private void initMatrix(int[,] matrix)
        {
            Matrix = new int[Size, Size];

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Matrix[i, j] = matrix[i, j];
                }
            }
        }

        private void initVisited(bool[] visited)
        {
            Visited = new bool[Size];

            for (int i = 0; i < Size; i++)
            {
                Visited[i] = visited[i];
            }

            Visited[Node] = true;
            Progress++;

        }
        public void ShowMatrix()
        {
            Console.WriteLine("Matrix dla node = " + Node);

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Console.Write(Matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        private void ReduceMatrix()
        {
            //po wierszach
            for (int i = 0; i < Size; i++)
            {
                int minValue = Int32.MaxValue;

                for (int j = 0; j < Size; j++)
                {
                    if (Matrix[i, j] < minValue)
                    {
                        minValue = Matrix[i, j];
                    }
                }

                if(minValue != 0 && minValue != Int32.MaxValue)
                {
                    Cost += minValue;
                    for (int j = 0; j < Size; j++)
                    {
                        if(Matrix[i, j] != Int32.MaxValue)
                        {
                            Matrix[i, j] = Matrix[i, j] - minValue;
                        }
                    }
                }
            }

            //po kolumnach
            for (int i = 0; i < Size; i++)
            {
                int minValue = Int32.MaxValue;

                for (int j = 0; j < Size; j++)
                {
                    if (Matrix[j, i] < minValue)
                    {
                        minValue = Matrix[j, i];
                    }
                }

                if (minValue != 0 && minValue != Int32.MaxValue)
                {
                    Cost += minValue;
                    for (int j = 0; j < Size; j++)
                    {
                        if (Matrix[j, i] != Int32.MaxValue)
                        {
                            Matrix[j, i] = Matrix[j, i] - minValue;
                        }
                    }
                }
            }
        }

        private void AddInfinityToMatrix()
        {
            for (int i = 0; i < Size; i++)
            {
                Matrix[ParentNode, i] = Int32.MaxValue;
            }

            for (int i = 0; i < Size; i++)
            {
                Matrix[i, Node] = Int32.MaxValue;
            }

            Matrix[Node, 0 ] = Int32.MaxValue;
        }

        public bool[] GetVisited()
        {
            return Visited;
        }

        public int[,] GetMatrix()
        {
            return Matrix;
        }


    }
}
