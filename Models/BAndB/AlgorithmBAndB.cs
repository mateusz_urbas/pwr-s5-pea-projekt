﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Models.BAndB
{
    class AlgorithmBAndB
    {
        public int[,] Matrix { get; set; }
        public int Size { get; set; }
        private int Counter { get; set; }
        private ExploreNode LastNode { get; set; }
        public bool Test { get; set; }

        public List<ExploreNode> ExploreNodes { get; set; }

        public AlgorithmBAndB(int[,] matrix, int size, bool test = false)
        {
            ExploreNodes = new List<ExploreNode>();

            Size = size;
            Matrix = new int[size, size];
            Matrix = matrix;
            Counter = 0;
            Test = test;
            TransformZeroToInfinity();
        }

        public void Start()
        {
            ExploreNodes.Add(new ExploreNode(getMatrix(), initVisited(), Size));
            Counter++;
            bool next = true;
            while (next)
            {
                next = AddNewNodes(FindMinWeightNode());
            }

            if(!Test)
            {
                ShowResult();
            }
        }

        private bool[] initVisited()
        {
            bool[] result = new bool[Size];
            for (int i = 0; i < Size; i++)
            {
                result[i] = false;
            }
            return result;
        }

        private void TransformZeroToInfinity()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (Matrix[i, j] == 0) Matrix[i, j] = Int32.MaxValue;
                }
            }
        }

        private ExploreNode FindMinWeightNode()
        {
            ExploreNode result = ExploreNodes.Find(a => a.IsExplored == false);
            ExploreNodes.FindAll(a => a.IsExplored == false).ForEach(b =>
            {
                if(result.Cost > b.Cost)
                {
                    result = b;
                }
            });
            //Console.WriteLine("Szukanie min...");
            //Console.WriteLine("... pozycja nr " + result.Position);
            //Console.ReadKey();

            return result;
        }

        private bool AddNewNodes(ExploreNode node)
        {
            node.IsExplored = true;
            bool[] list = node.GetVisited();
            bool result = true;
            for (int i = 0; i < Size; i++)
            {
                if(list[i] == false)
                {
                    ExploreNode newNode = new ExploreNode(Counter, node.Node, i, node.GetMatrix(), node.Cost, list, Size, node.Progress, node.Position);
                    ExploreNodes.Add(newNode);
                    Counter++;
                    if(newNode.Progress == Size)
                    {
                        result = false;
                    }
                }
            }

            if(result == false)
            {
               ExploreNode lastNode = FindMinWeightNode();
               if (lastNode.Progress != Size)
                {
                    result = true;
                }
               else
                {
                    LastNode = lastNode;
                }
            }
            return result;
        }

        public void ShowList()
        {
            ExploreNodes.ForEach(a =>
            {
                Console.WriteLine("Pozycja " + a.Position + " | " +
                    "Node "+ a.Node + " | " +
                    "ParentNode " + a.ParentNode + " | " +
                    "Koszt " + a.Cost + " | " +
                    "Progres " + a.Progress + "/" + Size);

            });
            Console.ReadKey();

        }

        private int[,] getMatrix()
        {
            return Matrix;
        }

        public void ShowResult()
        {
            Console.WriteLine();

            Console.WriteLine("Najkrótsza droga ma wartość: " + LastNode.Cost);

            List<int> road = new List<int>();

            road.Add(0);
            road.Add(LastNode.Node);

            bool next = true;
            ExploreNode temp = LastNode;

            while (next)
            {
                temp = findNode(temp.ParentPosition);
                road.Add(temp.Node);
                if (temp.Node == 0)
                {
                    next = false;
                }

            }

            for (int i = road.Count - 1; i >= 0; i--)
            {
                if(i != 0)
                {
                    Console.Write(road[i] + " --> ");
                }
                else
                {
                    Console.Write(road[i]);
                }
            }

            Console.ReadKey();
        }

        private ExploreNode findNode(int number)
        {
            return ExploreNodes.Find(a => a.Position.Equals(number));
        }

    }
}
