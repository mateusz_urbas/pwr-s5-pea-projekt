﻿using System;
using System.Collections.Generic;
using System.IO;
using Project2.Models;
using Project2.Models.BAndB;
using Project2.Models.BF;
using Project2.Models.DP2;

namespace Project2
{
    class World
    {
        public int[,] Matrix { get; set; }
        public int Size { get; set; } = 0;

        public void RunProject()
        {
            bool show = true;
            while (show)
            {
                show = MainMenu();
            }
        }

        private bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Wybierz opcję: \n" +
                              "1 -> Wyświetl Matrix \n" +         
                              "2 -> Algorytm B&B\n" +
                              "3 -> Algorytm DP \n" +
                              "4 -> Algorytm BF \n" +
                              "5 -> Wyjscie \n" +
                              "6 -> Testy \n" +
                              "7 -> Załaduj Matrix \n");
            int value = Int32.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

            bool show = true;
            switch (value)
            {
                case 1:
                    showMatrix();
                    break;
                case 2:
                    AlgorithmBAndB algorithmBAndB = new AlgorithmBAndB(Matrix, Size);
                    algorithmBAndB.Start();
                    break;

                case 3:
                    AlgorithmDP2 algorithmDP = new AlgorithmDP2(Matrix, Size);
                    algorithmDP.Start();
                    break;

                case 4:
                    AlgorithmBF algorithmBF = new AlgorithmBF(Matrix, Size);
                    algorithmBF.Start();
                    break;

                case 5:
                    return false;


                case 6:
                    while (show)
                    {
                        show = makeTest();
                    }
                    break;

                case 7:
                    FileRead();
                    break;
                default:
                    break;
            }

            return true;
        }


        bool makeTest()
        {
            TestWorld testWorld = new TestWorld();
            Console.Clear();
            Console.WriteLine("Wybierz opcję: \n" +
                              "1 -> BF \n" +
                              "2 -> B&B \n" +
                              "3 -> DP \n" +
                              "4 -> Wyjście");
            int value = Int32.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
            switch (value)
            {
                case 1:
                    testWorld.StartBF();
                    break;

                case 2:
                    testWorld.StartBAndB();
                    break;

                case 3:
                    testWorld.StartDP();
                    break;

                case 4:
                    return false;
            }

            return true;
        }

        public void FileRead()
        {
            Console.WriteLine("Podaj nazwę pliku");
            string fileName = Console.ReadLine();
            //string fileName = "t0";

            try
            {
                using (StreamReader stream = new StreamReader("../../Database/" + fileName + ".txt"))
                {
                    Console.WriteLine("Wczytywanie danych...");
                    String line = stream.ReadLine();

                    if (line != null)
                    {
                        Size = Int32.Parse(line);
                        Matrix = new int[Size, Size];

                        for (int i = 0; i < Size; i++)
                        {
                            line = stream.ReadLine();

                            if (line != null)
                            {
                                string[] infoStrings = line.Split(' ');
                                for(int j = 0; j < Size; j++)
                                {
                                    Matrix[i, j] = Int32.Parse(infoStrings[j]);
                                }
                            }
                        }
                    }

                    Console.WriteLine("...Wczytywanie zakończone.");
                    showMatrix();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Nie można otworzyć pliku");
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void showMatrix()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j=0; j<Size; j++)
                {
                    Console.Write(Matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

        }
    }
}